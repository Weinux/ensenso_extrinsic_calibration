 [![Institut Maupertuis logo](http://www.institutmaupertuis.fr/media/gabarit/logo.png)](http://www.institutmaupertuis.fr) Institut Maupertuis additive manufacturing
===

[![build status](https://gitlab.com/InstitutMaupertuis/ensenso_extrinsic_calibration/badges/kinetic/build.svg)](https://gitlab.com/InstitutMaupertuis/ensenso_extrinsic_calibration/commits/kinetic)

# Project description
This package allows to automatically calibrate the Ensenso on the robot by moving around a target.

- [This video](https://youtu.be/2g6gdx8fKX8) shows the calibration on the real robot.
- The [tutorial](documentation/tutorial.md) folder contains explanations about how to use this package.

<img src="/documentation/01.png" align="center" height="300">

The package was designed to be used with a Fanuc R1000iA-80f robot however it should be easy to port it
on other ROS compatible robots; refer to the [last section](README.md#changing-the-setup).

# Compiling
## Create a catkin workspace and clone the project:
```bash
mkdir -p $HOME/code/catkin_workspace
cd $HOME/code/catkin_workspace
git clone https://gitlab.com/InstitutMaupertuis/ensenso_extrinsic_calibration.git
```

## Resolve ROS dependencies

```bash
rosdep install --from-paths src --ignore-src --rosdistro kinetic -y
```

## Compile and install
Use `catkin_make`:
```bash
catkin_make
catkin_make install
```

Or `catkin tools`:
```bash
catkin config --install
catkin build
```

## Calibration plate
You can generate one and print it with `nxCalTab` which comes installed with the EnsensoSDK.
Export the generated target as a PDF and print it, make sure the scale is right!

## Launching
Refer to the [tutorial](documentation/tutorial.md).

## Changing the setup/robot
If you want to use this package with an other robot, an other end effector or an other camera setup (eg: camera is fixed) this is what you should care about:
- Change the [`calibration.launch`](/launch/calibration.launch) file to match your robot definition.
- Define the `ensenso_n10` manipulator in your MoveIt package and `ensenso_n10_tcp` frame, another option is to change them in the [node](/src/ensenso_extrinsic_calibration.cpp#L26-L31).
- Change the calibration parameters in the [node](/src/ensenso_extrinsic_calibration.cpp#L310) (eg: fixed instead of moving)
